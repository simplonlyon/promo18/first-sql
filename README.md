# first-sql
Projet avec les requêtes SQL de base commentées (fait avec [cette extension](https://marketplace.visualstudio.com/items?itemName=cweijan.vscode-mysql-client2))


## Exemple de Script SQL
Dans le fichier [clean-example](clean-example.sql), vous avez un exemple de ce à quoi peut ressembler un script sql pour un vrai projet.
L'idée est d'avoir un fichier qu'on peut importer facilement sur une base de données pour remettre celle-ci dans un état satisfaisant (avec les tables créées et un jeu de données). Le fichier commence donc par remettre la bdd à zéro en faisant un DROP des différentes tables (dans le bon ordre, vis à vis des foreign key) avant de les recréer puis de faire les insertions.