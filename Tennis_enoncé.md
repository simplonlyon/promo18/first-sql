
# Exercice d'entrainement : gestion d'un club de tennis 


##  Partie 1 - Préparation
### Question 1 : Créer une base de données "extennis"

### Question 2 : Création d'un utilisateur
Si vous ne l'avez pas encore, créez un utilisateur "simplon", avec comme mot de passe "1234"
Hint : https://dev.mysql.com/doc/refman/8.0/en/create-user.html

### Question 3 : Gestion des droits
Donnez tous les droits à l'utilisateur simplon sur la base extennis
Hint : https://dev.mysql.com/doc/refman/8.0/en/grant.html

### Question 4 : Rapport
Préparez un support pour noter vos réponses aux questions : readme dans un projet, google doc, fichier texte, ou autre, tant que vous êtes à l'aise pour vous repérer dedans !

## Partie 2 - Tables
### Question 1 : Le club
Un club de tennis est représenté par un identifiant auto-incrémenté, un nom, une adresse (varchar), un code postal (sur 5 chiffres), et une ville. Créez la table "club".
Insérez au moins 3 clubs dans la base, dont 2 à Lyon.

### Question 2 : Les terrains
Une surface de terrain est représentée par un identifiant numérique auto-incrémenté, et un libellé.
Un terrain de tennis est représenté par un identifiant numérique auto-incrémenté, un nom, et une surface. Un terrain est rattaché à un club.
Créez les 2 tables "surface_terrain" et "terrain".
Insérez trois terrains pour le club n°3, deux en dur et un en terre battue. Insérez au moins un terrain pour les autres clubs.

### Question 3 : Les joueurs
Un joueur est représenté par un numéro de licence, un genre (homme ou femme), un nom, et une catégorie (senior, vétéran, jeune, enfant). Un joueur peut être rattaché à un club, mais ce n'est pas obligatoire, il peut avoir une licence libre.
Hint : https://dev.mysql.com/doc/refman/8.0/en/enum.html

Créez la table "joueur", et insérez quelques joueurs (au moins 2 hommes senior et une femme senior).

### Question 4 : Les compétitions
Une compétition est représentée par un identifiant numérique auto-incrémenté, un nom, un club organisateur, et une catégorie. Une compétition peut accueillir plusieurs joueurs, et un joueur peut participer à plusieurs compétition.
Créez la table "competition". Insérez au moins 2 compétitions. 

### Question 5 : Requêtes
1. Écrivez la requête qui permet de sélectionner tous les clubs de Lyon
2. Écrivez la requête qui permet d'afficher tous les terrains du club n°3
3. Écrivez la requête qui permet d'afficher le nom et la surface du terrain n°2
4. Écrivez la requête qui permet d'afficher tous les terrains d'un club, avec leur surface. On doit voir le nom du club, le nom du terrain, et le libellé de la surface. Hint : pensez aux alias !
5. Avancé: affichez la liste de tous les clubs basés dans le Rhône (département 69).
6. Afficher, pour toutes les compétitions, le nom de la compétition, et le nom de tous les joueurs qui y ont participé.
7. Afficher, pour la compétition n°2, le nom de la compétition, et le nom de tous les terrains du club organisateur.
8. Afficher, pour la compétition n°2, le nom de la compétition, et le nom de tous les terrains du club organisateur avec le label de leur surface.
9. Afficher, pour chaque joueur, la liste des surfaces sur lesquelles il a déjà joué en compétition.


## Partie 3 - Pour aller plus loin 

### Question 1 : Les matches
Un match est représenté par un identifiant numérique auto-incrémenté, une date, un terrain, deux joueurs, et un score.
Créez la table "match". Insérez au moins 4 matches aléatoires entre les joueurs que vous avez créés. Laissez au moins 1 match en cours (sans score).

### Question 2 : Requêtes
1. Écrivez la requête permettant d'afficher tous les adversaires rencontrés par le joueur 2. Chaque adversaire ne peut apparaître qu'une seule fois. Triez la liste par nom des adversaires.
2. La FFT impose de ne jouer qu'un seul match par jour et par terrain. Écrivez la requête permettant d'afficher les erreurs d'organisation : 2 matches se déroulent le même jour sur le même terrain.
3. Suite de la question 2 : affichez le nom des clubs ne respectant pas la règle de la fédé.
4. Affichez la liste des matches mixtes (les deux joueurs n'ont pas le même genre)
5. Affichez la liste des matches opposant des joueurs de même genre mais de catégories différentes
6. Affichez la liste des joueurs ayant participé à un match sur terre battue


## Partie 4 - JDBC
### Question 1 : Lien Java/Base
1. Créez un nouveau projet Java "Tennis".
2. Créez une classe "Reporting" contenant une méthode run(), que vous appellerez depuis le main() de votre programme.
3. Créez une méthode connect() dans votre classe Reporting, et ouvrez une connexion à la base de données. Hint: https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-usagenotes-connect-drivermanager.html
4. Créez une méthode "disconnect()" dans votre classe Reporting; cette méthode doit fermer la connexion à la base.
5. Créez une méthode "showAllClubs()", et affichez dans la console la liste des clubs de Lyon (utilisez la requête de la partie 2 question 3.1)

## Partie 5 - DAO et Repository
### Question 1 : Entités
Créez une classe Java par table présente dans la base.
**Bonne pratique :** regroupez les entités dans un package "entity".

### Question 2 : DAO
Une DAO est une classe Java qui va lire les informations dans la table correspondant à l'entité.
Pour chaque entité, créez une classe <Entité>DAO, par exemple "ClubDAO", "TerrainDAO", etc.
**Bonne pratique :** regroupez les DAO dans un package "dao".
Dans les DAO club, surface, et terrain, ajoutez les méthodes "findById()", "findAll()", "create()", "update()", et "delete()". Les méthodes doivent renvoyer un objet du type correspondant.
Exemple, ClubDAO va contenir une méthode "public Club findById(int id)", et une méthode "public List\<Club> findAll()".
Testez dans votre code le chargement et l'affichage d'un terrain (Hint : surchargez la méthode toString() de la classe Terrain).

### Question 3 : Repository
Un repository est une classe Java qui va permet de faire le lien entre plusieurs tables.
Par exemple, la classe Terrain va avoir un membre de type Surface. La classe TerrainDAO ne va pas lire la surface (car elle n'est pas dans la classe Terrain, mais dans la classe Surface).
La classe TerrainRepository va utiliser les classes TerrainDAO et SurfaceDAO, pour renvoyer une entité Terrain complète.
Créez la classe TerrainRepository, qui va contenir une méthode "findById(int id)", et qui va renvoyer une entité Terrain complète (c'est-à-dire avec la surface).
**Bonne pratique :** regroupez les repository dans un package "repository".
Testez dans votre code le chargement et l'affichage d'un terrain complet (Hint : surchargez la méthode toString() de la classe Terrain).
