-- Question 1
CREATE TABLE game (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `date` DATE, -- le nom date est un mot-clé, il faut l entourer par des `` (AltGR+7)
    id_court INT UNSIGNED, -- on copie le type de court(id)
    player1 INT UNSIGNED, -- ou player1 VARCHAR(64) si vous avez choisi la license comme clé
    player2 INT UNSIGNED, -- ou player1 VARCHAR(64) si vous avez choisi la license comme clé
    score VARCHAR(32),
    CONSTRAINT FK_game_player_1 FOREIGN KEY (player1) REFERENCES player(id),
    CONSTRAINT FK_game_player_2 FOREIGN KEY (player2) REFERENCES player(id)
); -- Les clés étrangères player1 et player2 référencent la même table !

INSERT INTO game (`date`, id_court, player1, player2, score)
    VALUES
        ('2022-04-03', 6, 6, 5, '6-4 6-4 6-7 7-6'),
        ('2022-03-26', 7, 6, 7, '6-4 6-4 6-7 7-6'),
        ('2022-04-03', 6, 8, 9, NULL),
        ('2022-04-02', 9, 6, 5, '6-4 6-4 6-7 7-6'),
        ('2022-04-01', 8, 6, 10, '6-4 6-4 6-7 7-6');

-- Question 2
-- 2.1
SELECT DISTINCT player.name
    FROM player, game
    WHERE (game.player1 = 2 AND player.id = game.player2)
    OR (game.player2 = 2 AND player.id = game.player1);
-- EXPLICATION : ici, impossible de faire une jointure, on doit filtrer les 2 possibilités

-- 2.2
SELECT g.id, g2.id, g.id_court, g2.id_court, g.`date`, g.player1, g.player2, g2.player1, g2.player2
    FROM game g
    INNER JOIN game g2
        ON g.id_court = g2.id_court
    WHERE g.date = g2.date AND g.id <> g2.id;
-- EXPLICATION : on a tout à fait le droit de joindre une table à elle-même, à condition d'utiliser des alias !

-- 2.3
SELECT DISTINCT c.name AS CLUB
    FROM club c
    INNER JOIN court crt
        ON crt.id_club = c.id
    INNER JOIN game g
        ON g.id_court = crt.id
    INNER JOIN game g2
        ON g.id_court = g2.id_court
    WHERE g.date = g2.date AND g.id <> g2.id;
-- EXPLICATION : même requête que la précédente, en ajoutant la jointure vers la table CLUB pour récupérer son nom

-- 2.4 
SELECT g.id
    FROM game g
    INNER JOIN player p1
        ON p1.id = g.player1
    INNER JOIN player p2
        ON p2.id = g.player2
    WHERE p1.gender <> p2.gender;
-- EXPLICATION 1 : comme on a 2 clés étrangères vers la table 'player', et qu'on veut récupérer les informations des 2 joueurs, on DOIT faire les 2 jointures : une fois pour les infos du joueur1, une fois pour les infos du joueur2
-- EXPLICATION 2 : on peut filtrer dans le WHERE sur des champs contenus dans n'importe quelle table liée, pas forcément la table du "FROM"


-- 2.5
SELECT g.id, p1.name, c1.label, p2.name, c2.label
    FROM game g
    INNER  JOIN player p1
        ON p1.id = g.player1
    INNER JOIN category c1
        ON c1.id = p1.id_category
    INNER JOIN player p2
        ON p2.id = g.player2
    INNER JOIN category c2
        ON c2.id = p2.id_category
    WHERE p1.gender = p2.gender
        AND p1.id_category <> p2.id_category;
-- EXPLICATION : comme pour la requête précédente, on est obligés de faire des jointures sur 2 "chemins parallèles" pour atteindre la catégorie de chacun des joueurs

-- 2.6
SELECT p.name
    FROM player p, game g, court c, surface s
    WHERE s.label = 'terre battue'
        AND c.id_surface = s.id
        AND g.id_court = c.id
        AND (p.id = g.player1 OR p.id = g.player2);
