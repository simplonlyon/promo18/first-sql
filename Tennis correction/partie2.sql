-- Question 1 - Club
CREATE TABLE club (
    id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64),
    address VARCHAR(255),
    zipcode CHAR(5),
    city VARCHAR(255)
);

INSERT 
    INTO club (name, address, zipcode, city) 
    VALUES 
        ('US Lyon', '1 route des champs', '69007', 'Lyon'),
        ('FC Villeurbanne', '1 cours Émile Zola', '69100', 'Villeurbanne'),
        ('Tennis club de Lyon', '360 avenue Jean Jaurès', '69009', 'Lyon');

-- Question 2 - Surfaces et Terrains
CREATE TABLE surface (
    id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    label VARCHAR(45)
);
INSERT 
    INTO surface (label) 
    VALUES 
        ('dur'),
        ('gazon'),
        ('moquette'),
        ('terre battue');

CREATE TABLE court(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(150),
    id_surface TINYINT UNSIGNED,
    FOREIGN KEY (id_surface) REFERENCES surface(id)
);
-- On rattache le court à un club
ALTER TABLE court ADD COLUMN (id_club SMALLINT UNSIGNED);
ALTER TABLE court ADD CONSTRAINT FK_court_club FOREIGN KEY (id_club) REFERENCES club(id);

INSERT
    INTO court(name, id_surface, id_club) 
    VALUES
        ('Le joli court', 1, 3),
    ("Le Court du Grelot", 1, 3),
    ("Messieurs ! Le court", 4, 3),
    ("Court forest !", 2, 1),
    ("Court pour ta vie", 3, 2);

-- Question 3 - Joueurs
CREATE TABLE category (
    id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    label VARCHAR(64)
);
INSERT INTO category (label) 
    VALUES ('senior'), ('vétéran'), ('jeune'), ('enfant');

CREATE TABLE player (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64),
    gender BIT(1),
    id_category TINYINT UNSIGNED,
    license VARCHAR(64) UNIQUE,
    id_club SMALLINT UNSIGNED,
    CONSTRAINT FK_player_category FOREIGN KEY (id_category) REFERENCES category(id),
    CONSTRAINT FK_player_club FOREIGN KEY (id_club) REFERENCES club(id)
);
-- Alternative :
CREATE TABLE player (
    license VARCHAR(64) PRIMARY KEY,
    name VARCHAR(64),
    gender BIT(1),
    id_category TINYINT UNSIGNED,
    id_club SMALLINT UNSIGNED,
    CONSTRAINT FK_player_category FOREIGN KEY (id_category) REFERENCES category(id),
    CONSTRAINT FK_player_club FOREIGN KEY (id_club) REFERENCES club(id)
);
-- Explication : le numéro de licence (qui peut contenir des lettres) est unique pour chaque joueur, on peut donc l'utiliser comme clé primaire
-- Alternative avec ENUM
CREATE TABLE player (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64),
    gender ENUM('HOMME', 'FEMME', 'NON PRECISE'),
    id_category TINYINT UNSIGNED,
    license VARCHAR(64) UNIQUE,
    id_club SMALLINT UNSIGNED,
    CONSTRAINT FK_player_category FOREIGN KEY (id_category) REFERENCES category(id),
    CONSTRAINT FK_player_club FOREIGN KEY (id_club) REFERENCES club(id)
);

INSERT 
    INTO player(name, gender, id_category, id_club, license)
    VALUES
        ("Roger Federer", 1, 1, 7,"fdsfze45"),
        ("Rafa NADAL", 1, 1 , 7,"fdsfze46"),
        ("Stan Waw", 1, 1, 8,"fdsfze47"),
        ("Novak Djokovic", 1, 1, 9,"fdsfze48"),
        ("Serena Williams", 0, 2, 9, "fdsfze49"),
        ("Pete Sampras", 1, 2, 7, "fdsfze50");

-- Question 4 - Compétitions
CREATE TABLE competition (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(128),
    id_club SMALLINT UNSIGNED,
    id_category TINYINT UNSIGNED,
    max_players TINYINT UNSIGNED,
    CONSTRAINT FK_competition_category FOREIGN KEY (id_category) REFERENCES category(id),
    CONSTRAINT FK_competition_club FOREIGN KEY (id_club) REFERENCES club(id)
);

INSERT 
    INTO competition(name, id_club, id_category, max_players)
    VALUES
        ("French Open", 1, 1, 64),
        ("Australian Open", 3, 1, 128),
        ("US Open", 2, 1, 128),
        ("Wimbledon", 3, 2, 32);

--  Lien entre compétition et joueur
-- Comme on a une relation many-to-many, on doit créer une table de jointure pour faire le lien
CREATE TABLE competition_player_joint (
    id_competition INT UNSIGNED,
    id_player INT UNSIGNED,
    CONSTRAINT PK_competition_player PRIMARY KEY (id_competition, id_player),
    CONSTRAINT FK_competitionplayer_competition FOREIGN KEY (id_competition) REFERENCES competition(id),
    CONSTRAINT FK_competitionplayer_player FOREIGN KEY (id_player) REFERENCES player(id)
);
INSERT
    INTO competition_player_joint(id_competition, id_player)
    VALUES
        (1, 1),
        (2, 1),
        (3, 4),
        (2, 4),
        (3, 1),
        (1, 4),
        (1, 2),
        (3, 2);


-- Question 5  - Requêtes
-- 5.1
SELECT *
    FROM club
    WHERE city = 'Lyon';

-- 5.2 
SELECT name
    FROM court 
    WHERE id_club = 3;

-- 5.3
SELECT court.name, surface.label 
    FROM court, surface 
    WHERE court.id = 2 
        AND surface.id = court.id_surface;
-- Explication : Je sélectionne le nom du court, le nom de la surface pour le court n°2, et la surface associée à CE cours
--
-- 5.3 alternative : Syntaxe avec JOIN
SELECT court.name, surface.label
    FROM court
        INNER JOIN surface 
            ON surface.id = court.id_surface
    WHERE court.id = 2;

-- 5.4
-- Hypothèse : On a choisi le club n°3
SELECT club.name AS CLUB_NAME, court.name AS COURT_NAME, surface.label AS TYPE_SURFACE
    FROM club, court, surface
    WHERE club.id = 3
        AND court.id_club = club.id
        AND surface.id = court.id_surface;
-- Explication : Je sélectionne le nom du club que je veux afficher dans une colonne CLUB_NAME, le nom du court que je veux afficher dans une colonne COURT_NAME, le label de la surface que je veux afficher dans une colonne TYPE_SURFACE, pour le club n°3, les terrains de CE club, et les surfaces de CES terrains
--
-- Alternative : Syntaxe JOIN
SELECT club.name AS CLUB_NAME, court.name AS COURT_NAME, surface.label AS TYPE_SURFACE
    FROM club
        INNER JOIN court
            ON court.id_club = club.id
        INNER JOIN surface
            ON surface.id = court.id_surface
    WHERE club.id = 3;

-- 5.5
SELECT *
    FROM club
    WHERE zipcode LIKE '69%';

-- 5.6
SELECT c.name AS TOURNAMENT, p.name AS PLAYER
    FROM competition c
    INNER JOIN competition_player_joint cp
        ON cp.id_competition = c.id
    INNER JOIN player p
        ON p.id = cp.id_player;
    -- Alias de tables : on met FROM <nom_de_la_table> <alias> pour raccourcir l'écriture de la requête

-- 5.7 
SELECT co.name AS TOURNAMENT, crt.name AS COURT_NAME
    FROM competition co
    INNER JOIN club cl
        ON cl.id = co.id_club
    INNER JOIN court crt
        ON crt.id_club = cl.id;

-- 5.8
SELECT co.name AS TOURNAMENT, crt.name AS COURT_NAME, s.label AS TYPE_SURFACE
    FROM competition co
    INNER JOIN club cl
        ON cl.id = co.id_club
    INNER JOIN court crt
        ON crt.id_club = cl.id
    INNER JOIN surface s
        ON s.id = crt.id_surface;

-- 5.9
SELECT p.name AS PLAYER, s.label as SURFACE
-- SELECT DISTINCT p.name AS PLAYER, s.label as SURFACE -- pour ne pas afficher de ligne en double
    FROM player p
    INNER JOIN competition_player_joint cp
        ON cp.id_player = p.id
    INNER JOIN competition co
        ON co.id = cp.id_competition
    INNER JOIN club cl
        ON cl.id = co.id_club
    INNER JOIN court crt
        ON crt.id_club = cl.id
    INNER JOIN surface s
        ON s.id = crt.id_surface
    ORDER BY PLAYER;