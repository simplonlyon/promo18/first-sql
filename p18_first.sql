-- Créer une base de données
-- CREATE DATABASE p18_first;

-- Place le terminal sur la base de données indiquée
-- USE p18_first;
/* 
Exemple de création de table dans notre base de données.
Une table SQL représentera les données à faire persister pour une "entité" (exemples d'entités : produit, user, commentaire, article, etc.)
Chaque table devra posséder une clef primaire (PRIMARY KEY) qui sera un identifiant
unique pour les données de cette table. Ici on l'a mis en AUTO_INCREMENT pour que cet id
soit généré automatiquement par la bdd.
Toutes les colonnes d'une table doivent posséder un type (qui correspondent plus ou moins
aux types primitifs du Java)
*/
CREATE TABLE person (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64),
    first_name VARCHAR(64),
    birth_date DATE
);

CREATE TABLE address (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    number VARCHAR(8),
    street VARCHAR(128),
    city VARCHAR(64),
    postal_code VARCHAR(8),
    id_person INTEGER,
    /*
    Une FOREIGN KEY permet d'indiquer qu'une colonne fait référence à la colonne
    d'une autre table. Cela permettra de créer des relations entre les tables.
    Ici, on dit que le champ id_person de l'address fait référence à l'id de la table person.
    Cela signifie qu'une person pourra possèder plusieurs addresses et qu'une address ne
    pourra apartenir qu'à une seule person.
    */
    FOREIGN KEY (id_person) REFERENCES person(id)
    -- Si je supprime une personne, ça supprime toutes ses adresses
    -- FOREIGN KEY (id_person) REFERENCES person(id) ON DELETE CASCADE
    -- Si je supprime une personne, ça met le champ id_person de ses adresses à null
    -- FOREIGN KEY (id_person) REFERENCES person(id) ON DELETE SET NULL
);

CREATE TABLE skill (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(64)
);

/* Pour créer une relation Many To Many entre la table skill et la table person
il faut créer une "table de jointure" dont l'objectif sera de faire le lien entre
les deux tables visées. Cette table ne contiendra que des FOREIGN KEY qui seront 
également PRIMARY KEY.
Ici, notre table fait donc référence à l'id de la table person ainsi qu'à l'id de la 
table skill pour dire quelles personnes possèdent quelles compétences
*/
CREATE TABLE person_skill (
    id_person INT,
    id_skill INT,
    PRIMARY KEY (id_person, id_skill), -- ici on fait une clé primaire composite
    FOREIGN KEY (id_person) REFERENCES person(id),
    FOREIGN KEY (id_skill) REFERENCES skill(id)
    
);


-- Afficher les tables de la base de données actuelle
SHOW TABLES;

-- Afficher la structure d'une table
DESC address;

-- Rajouter une personne dans la table person
INSERT INTO person (name, first_name,birth_date) VALUES ("Bobson", "Bobby", "1990-11-23");
-- Faire en sorte d'afficher les personnes stockées dans la table person
SELECT * FROM person;
-- Affiche la personne dont l'id est égal à 1
SELECT * FROM person WHERE id=1;


-- Rajouter 3-4 person dont 2 qui s'appellent Jehane, une qui s'appelle Jean, et 2 qui sont nées après 1995
INSERT INTO person (name, first_name,birth_date) VALUES 
("Durol", "Jehane", "1996-10-23"), 
("Aprel", "Jehane", "1980-04-18"), 
("Bloupy", "Jean", "2000-03-03"), 
("Bloupo", "Bloup", "1998-02-04");
-- Faire un SELECT des person qui s'appellent Jehane
SELECT * FROM person WHERE first_name="Jehane";
-- Faire un SELECT des person nées après 1995
SELECT * FROM person WHERE YEAR(birth_date) > 1995;
SELECT * FROM person WHERE  birth_date >= "1996-01-01";
-- Faire un SELECT des person nées après 1995 et qui s'appellent Jehane
SELECT * FROM person WHERE YEAR(birth_date) > 1995 AND first_name="Jehane";
-- Faire un SELECT des person nées entre les années 1990 et 2000
SELECT * FROM person WHERE YEAR(birth_date) >= 1990 AND YEAR(birth_date) <= 2000;
SELECT * FROM person WHERE YEAR(birth_date) BETWEEN 1990 AND 2000;
-- Faire un SELECT des person dont le prénom commencent par "Je" (il faudra utiliser un LIKE)
SELECT * FROM person WHERE first_name LIKE "je%";

-- Mettre à la birth_date de la person avec l'id 3
UPDATE person SET birth_date="2000-01-01" WHERE id=3;
-- Suppression d'une person avec l'id 1
DELETE FROM person WHERE id=1;


INSERT INTO address (number, street, city, postal_code) VALUES 
("34", "rue Antoine Primat", "Villeurbanne", "69100"),
("5", "cours Gambetta", "Lyon", "69003"),
("150", "avenue de la république", "Lyon", "69002");

-- insertion d'address avec référence à la person
INSERT INTO address (number, street, city, postal_code, id_person) VALUES 
("34", "rue Antoine Primat", "Villeurbanne", "69100", 3);


SELECT * FROM address WHERE id_person=3;


-- Dans la table de jointure, on insert uniquement les id pour faire le lien entre les person et skill
INSERT INTO person_skill (id_person, id_skill) VALUES (1, 1);
INSERT INTO person_skill (id_person, id_skill) VALUES (1, 2);
INSERT INTO person_skill (id_person, id_skill) VALUES (2, 1);