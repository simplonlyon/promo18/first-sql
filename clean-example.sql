DROP TABLE IF EXISTS person_skill;
DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS skill;


CREATE TABLE person (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64),
    first_name VARCHAR(64),
    birth_date DATE
);

CREATE TABLE address (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    number VARCHAR(8),
    street VARCHAR(128),
    city VARCHAR(64),
    postal_code VARCHAR(8),
    id_person INTEGER,
    FOREIGN KEY (id_person) REFERENCES person(id)
   
);


CREATE TABLE skill (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(64)
);

CREATE TABLE person_skill (
    id_person INT,
    id_skill INT,
    PRIMARY KEY (id_person, id_skill), -- ici on fait une clé primaire composite
    FOREIGN KEY (id_person) REFERENCES person(id),
    FOREIGN KEY (id_skill) REFERENCES skill(id)
    
);

INSERT INTO person (name, first_name,birth_date) VALUES 
("Durol", "Jehane", "1996-10-23"), 
("Aprel", "Jehane", "1980-04-18"), 
("Bloupy", "Jean", "2000-03-03"), 
("Bloupo", "Bloup", "1998-02-04");

INSERT INTO address (number, street, city, postal_code, id_person) VALUES 
("34", "rue Antoine Primat", "Villeurbanne", "69100", 1),
("5", "cours Gambetta", "Lyon", "69003", 1),
("150", "avenue de la république", "Lyon", "69002", 3);

INSERT INTO person_skill (id_person, id_skill) VALUES (1, 1);
INSERT INTO person_skill (id_person, id_skill) VALUES (1, 2);
INSERT INTO person_skill (id_person, id_skill) VALUES (2, 1);