package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.promo18.entity.Person;
/**
 * Un repository (ou DAO) est une classe dont le but sera de faire des requêtes vers
 * la base de données afin de convertir des instances d'entités (Person par exemple)
 * en requête INSERT/UPDATE/DELETE ou l'inverse, convertir une requête SELECT en 
 * instance d'entité.
 * L'idée est de regrouper toutes les méthodes faisant appel à la BDD au mêmes endroits
 * pour faire que hors des repository il n'y ait pas du tout de référence à la BDD (et
 * ce afin d'avoir un code plus maintenable)
 */
public class PersonRepository {

    /**
     * La méthode findAll va faire une requête SELECT pour aller chercher toutes les
     * person actuellement contenues dans la table person de notre mysql, puis va itérer
     * sur les résultats et en faire des instances de l'entité Person
     * @return Une liste contenant les instances représentant les persons actuellement contenues dans la BDD
     */
    public List<Person> findAll() {
        List<Person> list = new ArrayList<>();
        try {
            /**
             * Ici on se connecte à la base de données en utilisant une string de connexion
             * standardisée décomposée comme suit : 
             * jbdc:sgbd://username:password@host:port/database
             */
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://simplon:1234@localhost:3306/p18_first");
            //Avec cette connexion, on prépare la requête qu'on va vouloir exécuter
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person");

            //On exécute la requête et on stock le résultat de celle ci dans un ResultSet
            ResultSet rs = stmt.executeQuery();

            //On boucle sur tous les résultats renvoyées et pour chaque résultat...
            while (rs.next()) {
                //...on crée une instance de l'entité Person dans laquelle on mettra les valeurs des colonnes
                Person person = new Person(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("first_name"),
                        rs.getDate("birth_date").toLocalDate());
                //Enfin, on ajoute la person à la liste que l'on va return
                list.add(person);
            }
            //Et on ferme notre connexion
            connection.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }
}
